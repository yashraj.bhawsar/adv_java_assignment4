<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" >
</head>

<style>

.container {
	margin-top: 100px;
}

.form-control {
	width: 400px;
}

.card-header {
	background-color: rgb(227, 242, 255);
}

.card-body {
	background-color: rgb(234, 246, 255);
}

.col-sm-6 {
	margin-left: 20px
}

.bulged {
	border: solid #6a87b2;
	border-radius: 5px;
}

.bulged:active {
	background-color: #d4eaff;
	box-shadow: 0 2px #d4eaff;
	transform: translateY(2px);
}

.card-footer {
	text-align: right;
	background-color: rgb(227, 242, 255);
}

</style>


<body>

	<form action ="login" method= post>

		<!--container-->

		<div class="container">

			<div class="card text-center">

				<!-- Header -->
				<div class="card-header">
					<ul class="nav nav-pills card-header-pills">
						<li class="nav-item" class="nav-link active" style="color: rgb(63, 86, 143);">
							<h4>
								<b>Login</b>
							</h4>
						</li>
					</ul>
				</div>
				
				<!-- Form body -->
				<div class="card-body">

					<div class="row g-6 align-items-center">
					
						<div class="col-auto">
							<label for="inputUser6" class="col-form-label"><b>Username:</b>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								&nbsp; <span style="color: #ff0000">*</span> </label>
						</div>
						
						<div class="col-auto">
							<input type="username" name="username" id="inputUsername6" class="form-control">
						</div>
						
					</div>

					<br>

					<div class="row g-10 align-items-center">
					
						<div class="col-auto">
							<label for="inputPassword6" class="col-form-label"><b>Password:</b>
								&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
								&nbsp; <span style="color: #ff0000">*&nbsp;</span> </label>
						</div>
						
						<div class="col-auto">
							<input type="password" name="password" id="inputPassword6" class="form-control">
						</div>
						
					</div>

					<br>

					<div class="forget-password">
						<div class="col-sm-6">
							<a href="#">Forgotten your password?</a>
						</div>
					</div>

		
				</div>

				<!-- Footer -->
				<div class="card-footer">

					<button type="submit" class="bulged">
						<b>Login >></b>
					</button>

				</div>

			</div>

		</div>
	</form>
</body>

</html>
	
