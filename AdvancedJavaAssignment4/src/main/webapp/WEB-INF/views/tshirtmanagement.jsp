<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page isELIgnored="false"%>
<%@page import="com.nagarro.tshirtfinder.model.TShirtModel"%>
<%@page import="java.io.*,java.util.*"%>


<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>T-Shirt Management</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
	integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js"
	integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
</head>
<body>



	<div class="container">

		<div class="card">
			<div class="card-header text-center"">
				<h4>T-Shirt Management Portal</h4>
			</div>

			<form action="tshirttable" method="post" class="card-body">
				<!-- Colour -->
				<div class="form-group row">
					<label for="colour" class="col-sm-2 col-form-label">Colour</label>
					<div class="col-sm-10">
						<input type="colour" class="col-md-4" id="colour" name="colour">
					</div>
				</div>

				<!-- Size -->
				<div class="form-group row">
					<label for="inputPassword3" class="col-sm-2 col-form-label">Size</label>

					<div class="col-sm-10">
						<div class="btn-group dropright">

							<select name="size">
								<option value="S">S</option>
								<option value="M">M</option>
								<option value="L">L</option>
								<option value="XL">XL</option>
								<option value="XXL">XXL</option>
							</select>
						</div>
					</div>
				</div>

				<!-- Gender -->

				<fieldset class="form-group">
					<div class="row">
						<legend class="col-form-label col-sm-2 pt-0">Gender</legend>
						<div class="col-sm-10">
							<div class="form-check custom-control-inline">
								<input class="form-check-input" type="radio" name="gender"
									id="male" value="M" checked> <label
									class="form-check-label" for="male">Male</label>
							</div>
							<div class="form-check custom-control-inline">
								<input class="form-check-input" type="radio" name="gender"
									id="female" value="F"> <label class="form-check-label"
									for="female">Female</label>
							</div>
						</div>
					</div>
				</fieldset>


				<!-- Sort by -->

				<fieldset class="form-group">
					<div class="row">
						<legend class="col-form-label col-sm-2 pt-0">Sort By</legend>
						<div class="col-sm-10">
							<div class="form-check custom-control-inline">
								<input class="form-check-input" type="radio" name="sort"
									id="price" value="price" checked> <label
									class="form-check-label" for="male">Price</label>
							</div>
							<div class="form-check custom-control-inline">
								<input class="form-check-input" type="radio" name="sort"
									id="rating" value="rating"> <label
									class="form-check-label" for="female">Rating</label>
							</div>
							<div class="form-check custom-control-inline">
								<input class="form-check-input" type="radio" name="sort"
									id="both" value="both"> <label class="form-check-label"
									for="female">Both Rating & Price</label>
							</div>
						</div>
					</div>
				</fieldset>

				<!-- submit button -->
				<div class="form-group row ">
					<div class="col-sm-10">
						<button type="submit" class="btn btn-success mr-4">Submit</button>
						<button type="reset"
							class="btn btn-warning  text-align-right ml-4"
							onClick="window.location.reload();">Clear</button>
					</div>
				</div>
			</form>

			<br>


			<!-- table -->


			<div class="container">
				<div class="card border-dark mb-4">
					<h3 class="text-center">T-Shirt Table</h3>
					<table
						class="col-sm-8  table table-bordered border-dark border-2 table-responsive-sm mb-4">
						<thead class="thead-dark">
							<tr class="text-center border-2 ">
								<th class="col-xs-2">ID</th>
								<th class="col-xs-2">NAME</th>
								<th class="col-xs-2">COLOUR</th>
								<th class="col-xs-2">GENDER</th>
								<th class="col-xs-2">SIZE</th>
								<th class="col-xs-2">PRICE</th>
								<th class="col-xs-2">RATING</th>
								<th class="col-xs-2">AVAILABILITY</th>
							</tr>
						</thead>


						<tbody class="text-center">
							<%
								@SuppressWarnings("unchecked")
								List<TShirtModel> tshirt_list = (List<TShirtModel>) request.getAttribute("tshirtdata");
								if (tshirt_list != null) {

									for (TShirtModel t : tshirt_list) {
							%>

							<tr class="text-center border-2 ">
								<td class="col-xs-2 border-dark ">
									<%
										out.print(t.getId());
									%>
								</td>
								<td class="col-xs-2 border-dark ">
									<%
										out.print(t.getName());
									%>
								</td>
								<td class="col-xs-2 border-dark ">
									<%
										out.print(t.getColour());
									%>
								</td>
								<td class="col-xs-2 border-dark ">
									<%
										out.print(t.getSize());
									%>
								</td>
								<td class="col-xs-2 border-dark ">
									<%
										out.print(t.getGender());
									%>
								</td>
								<td class="col-xs-2 border-dark ">
									<%
										out.print(t.getPrice());
									%>
								</td>
								<td class="col-xs-2 border-dark ">
									<%
										out.print(t.getRating());
									%>
								</td>
								<td class="col-xs-2 border-dark ">
									<%
										out.print(t.getAvailability());
									%>
								</td>
							</tr>

							<%
								}
								}

							%>
								<%-- else  {
							<tr>
								<td class = "center" colspan="8">
									<h4>No data Found!!</h4>
								</td>
							</tr>
							<%
								}
							%> --%>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>



<script type="text/javascript">
	$('.dropdown-item').click(function() {
		$('#selected').eq(0).text($(this).text());
	});
</script>

</html>