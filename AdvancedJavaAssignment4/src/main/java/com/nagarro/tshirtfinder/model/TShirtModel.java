package com.nagarro.tshirtfinder.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class TShirtModel {
	
	@Id
	private String id;
	private String name;
	private String colour;
	private String size;
	private String gender;
	private float price;
	private float rating;
	private String availability;
	

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColour() {
		return colour;
	}
	public void setColour(String colour) {
		this.colour = colour;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public float getRating() {
		return rating;
	}
	public void setRating(float rating) {
		this.rating = rating;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	
	@Override
	public String toString() {
		return "TShirtModel [id=" + id + ", name=" + name + ", colour=" + colour + ", size=" + size + ", gender="
				+ gender + ", price=" + price + ", rating=" + rating + ", availability=" + availability + "]";
	}
	
	


}
