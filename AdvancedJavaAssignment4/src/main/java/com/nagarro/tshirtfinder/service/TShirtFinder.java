package com.nagarro.tshirtfinder.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nagarro.tshirtfinder.dao.TShirt;
import com.nagarro.tshirtfinder.model.TShirtModel;

@Service
public class TShirtFinder {

	@Autowired
	private TShirt tshirt;
	

	public List<TShirtModel> getSearchResult(String colour, String size, String gender) {

		List<TShirtModel> result = new ArrayList<TShirtModel>();
		List<TShirtModel> dataCheck = tshirt.getAllData();


		for (TShirtModel val : dataCheck) {
			if (val.getColour().equals(colour) && val.getSize().equals(size) && val.getGender().equals(gender)) {
				result.add(val);
			}

		}
		return result;

	}

	

	public List<TShirtModel> tshirtSorting(List<TShirtModel> tshirtModel, String sortBy) {
		// sorting by price
		if (sortBy.equals("price")) {
			Comparator<TShirtModel> c1 = (TShirtModel m1, TShirtModel m2) -> (int) (m1.getPrice() - m2.getPrice());
			Collections.sort(tshirtModel, c1);
			
		}
		// sorting by rating
		else if (sortBy.equals("rating")) {
			Comparator<TShirtModel> c2 = (TShirtModel m1, TShirtModel m2) -> (int) (m1.getRating() - m2.getRating());
			Collections.sort(tshirtModel, c2);
			
		}

		// sorting by both price and rating
		else if (sortBy.equals("both")) {
			Comparator<TShirtModel> c3 = (TShirtModel m1, TShirtModel m2) -> (int) (m1.getPrice() - m2.getPrice())
					- (int) (m1.getRating() - m2.getRating());
			Collections.sort(tshirtModel, c3);
		}
		return tshirtModel;


	}

}
