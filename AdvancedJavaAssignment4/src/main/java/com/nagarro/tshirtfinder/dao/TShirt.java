package com.nagarro.tshirtfinder.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.tshirtfinder.model.TShirtModel;

@Repository
public class TShirt {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	static List<String> filenames = new ArrayList<String>();

	public static void getFiles() {
		File folder = new File(
				"C:\\Users\\yashrajbhawsar\\Desktop\\Advance Java\\Assigment Links\\Assigment Links\\csvfiles");
		//System.out.println(folder.getName());
		
		System.out.println(folder.listFiles());
		for (File fileEntry : folder.listFiles()) {
			if (fileEntry.getName().contains(".csv")) {
				filenames.add(fileEntry.getName());
			}
		}
		System.out.println(filenames);
	}

	static List<String> csv_data = new LinkedList<String>();

	@Transactional
	public List<String> saveFiles() {
		getFiles();
		try {
			if (filenames.size() > 0) {
				int i = 0;
				while (i < filenames.size()) {
					BufferedReader br = new BufferedReader(new FileReader(
							"C:\\Users\\yashrajbhawsar\\Desktop\\Advance Java\\Assigment Links\\Assigment Linkscsvfiles\\"
									+ filenames.get(i)));
					br.readLine();
					String line = "";
					while ((line = br.readLine()) != null) {
						if (!csv_data.contains(line)) {
							String[] temp = line.split("\\|");
							TShirtModel row = new TShirtModel();
							row.setId(temp[0]);
//							System.out.println(temp[0]);
							row.setName(temp[1]);
							row.setColour(temp[2]);
							row.setGender(temp[3]);
							row.setSize(temp[4]);
							row.setPrice(Float.parseFloat(temp[5]));
							row.setRating(Float.parseFloat(temp[6]));
							row.setAvailability(temp[7]);
							this.hibernateTemplate.save(row);
							csv_data.add(line);
						}

					}

					i++;
					br.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return csv_data;
	}

	public List<TShirtModel> getAllData() {
		// recived all the data here
		
		

		List<TShirtModel> tshirtDataList = new ArrayList<TShirtModel>();

		System.out.println("tshirt...................");
		try {

			List<TShirtModel> tshirtdata = hibernateTemplate.loadAll(TShirtModel.class);

			for (TShirtModel tm : tshirtdata) {
				//System.out.println(tm.getColour());
				tshirtDataList.add(tm);
			}
//			return tshirtDataList;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return tshirtDataList;

	}

}
