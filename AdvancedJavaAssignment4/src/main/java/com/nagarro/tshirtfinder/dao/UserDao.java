package com.nagarro.tshirtfinder.dao;



import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.nagarro.tshirtfinder.model.UserModel;



@Repository
public class UserDao{
     	
	@Autowired
     private SessionFactory sessionFactory;
       
       
       public boolean checkLogin(String userName, String userPassword){
    	   	Transaction transaction = null;
   			UserModel user = null;
			System.out.println("In Check login...................");
			try {
				Session session = sessionFactory.openSession();
				//Query using Hibernate Query Language
				
				transaction = session.beginTransaction();
				// get an user object
				user = (UserModel) session.createQuery("from tshirtlogin where username =:username").setParameter("username", userName).uniqueResult();
				
				System.out.println(user);
				
				if(user != null && user.getPassword().equals(userPassword)) {
					return true;
				}
				// commit transaction
				transaction.commit();
				session.close();              
			}
			catch(Exception e) {
				e.printStackTrace();
			}
			return false;


       }
}