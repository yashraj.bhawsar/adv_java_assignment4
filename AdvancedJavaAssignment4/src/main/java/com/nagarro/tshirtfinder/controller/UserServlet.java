package com.nagarro.tshirtfinder.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.nagarro.tshirtfinder.dao.UserDao;





@Controller
public class UserServlet{
	@Autowired
	private UserDao userDao;
	
	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String ShowForm() {
		System.out.println("Login page fired................");
		return "login";
	}
	
	@RequestMapping(path = "/login", method = RequestMethod.POST)
	public String handleForm(@RequestParam("username") String username, @RequestParam("password") String password) {
		
		System.out.println("login success..............");
		
		//System.out.println(user);

		try {
			
			if(userDao.checkLogin(username, password)) {
				// redirect to tshirt page
				System.out.println("true...................");
				return "tshirtmanagement";
				
			}
			else {
				//redirect login page
				System.out.println("false.....................");
				return "login";
			}
			
		}catch(Exception e) {
			
			System.out.println("catch..........................");
			e.printStackTrace();
			return "login";
		}
		
	
	}
	
	
}