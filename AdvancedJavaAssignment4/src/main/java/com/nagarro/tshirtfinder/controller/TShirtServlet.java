package com.nagarro.tshirtfinder.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.nagarro.tshirtfinder.service.TShirtFinder;
import com.nagarro.tshirtfinder.dao.TShirt;
import com.nagarro.tshirtfinder.model.TShirtModel;


@Controller
public class TShirtServlet {
	
	@Autowired
	private TShirtFinder tshirtFinder;
	
	
	@Autowired
	private TShirt tshirt;
	

	@RequestMapping(path = "/tshirttable", method = RequestMethod.POST)
	public String handleForm(@ModelAttribute TShirtModel tshirt, @RequestParam("sort") String sortBy,Model model ) {
		

		System.out.println("----------------------------------------");
		System.out.println(tshirt.getColour());
		System.out.println(tshirt.getSize());
		System.out.println(tshirt.getGender());
		System.out.println(sortBy);
		
//		tshirtDao.getFiles();
//		tshirtDao.saveFiles();
		
		List<TShirtModel> tshirtResult = tshirtFinder.getSearchResult(tshirt.getColour(),tshirt.getSize(),tshirt.getGender());
		//System.out.println(tshirtResult);
		List<TShirtModel> finalResult = tshirtFinder.tshirtSorting(tshirtResult,sortBy);
		System.out.println(finalResult);
		
		//tshirtDao.saveFiles();      // save the data in DB
		
		
		model.addAttribute("tshirtdata", finalResult);

		
		
		return "tshirtmanagement";
	}
	

	
}
